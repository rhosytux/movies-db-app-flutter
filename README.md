## Movies DB App

An example application displays a list of movies using flutter and uses the API from TMDB.
Tutorial : https://blog.usejournal.com/flutter-hands-on-building-a-movie-listing-app-using-flutter-part-1-c2b22d9be6b8

## Screnshot

![](screenshot/screnshot.jpeg)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
